use std::{any::Any, sync::Arc};

use tokio::task::JoinSet;

#[derive(Debug, Clone)]
pub struct UserData(i32, String);

fn fetch_user() -> UserData {
    UserData(3, String::from("hello world"))
}

fn service_id(val: i32) {
    println!("{:?}", val);
}
fn service_user_data_two(val: UserData) {
    println!("two: {:?}", val);
}
fn service_user_data(val: UserData) {
    println!("one: {:?}", val);
}

type ServiceParams = Box<dyn Any + Send + Sync + 'static>;
type Service = Arc<dyn Fn(ServiceParams) + Send + Sync + 'static>;
type Services = Vec<Service>;

pub struct Builder {
    services: Arc<Services>,
}

impl Builder {
    pub fn new() -> Self {
        Self {
            services: Default::default(),
        }
    }
    pub fn add<T: Clone + Any + Send + Sync>(&mut self, func: fn(T)) {
        let _service = Arc::new(move |value: ServiceParams| {
            if let Ok(val) = value.downcast::<T>() {
                func((*val).clone());
            }
        });
        Arc::get_mut(&mut self.services).unwrap().push(_service);
    }

    pub async fn payload<T: Clone + Any + Send + Sync + 'static>(&self, val: T) {
        let services = self.services.clone();
        let box_val: Box<T> = Box::new(val);
        let mut tasks = JoinSet::new();
        for svc in services.iter() {
            let box_val = box_val.clone();
            let svc_clone = svc.clone();
            tasks.spawn(async move {
                svc_clone(box_val);
            });
        }
        tasks.join_all().await;
    }
}

impl Default for Builder {
    fn default() -> Self {
        Self::new()
    }
}

#[tokio::main(flavor = "multi_thread")]
async fn main() {
    let mut builder = Builder::new();
    let users = fetch_user();
    builder.add(service_user_data);
    builder.add(service_id);
    builder.add(service_user_data_two);
    builder.payload(users.0).await;
    builder.payload(users).await;
}
